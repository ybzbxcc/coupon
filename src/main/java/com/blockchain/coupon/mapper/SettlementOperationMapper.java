package com.blockchain.coupon.mapper;

import java.util.List;

import com.blockchain.coupon.po.SettlementApplication;
import com.blockchain.coupon.po.SettlementOperationCustom;

public interface SettlementOperationMapper {	
//	结算券申请初审列表查询
	public List<SettlementApplication> querySFCList()throws Exception;
	
//	结算券申请复审列表查询 
	public List<SettlementApplication> querySSCList()throws Exception;
	

//	结算券提现初审列表查询
	public List<SettlementApplication> querySWDFList()throws Exception;
	
//	结算券提现复审列表查询 
	public List<SettlementApplication> querySWDSList()throws Exception;
	
//	更新结算券申请初审结果
	public void updateSettlementFirstCheck(SettlementOperationCustom settlementOperationCustom)throws Exception;
	
//	更新结算券申复审结果
	public void updateSettlementSecondCheck(SettlementOperationCustom settlementOperationCustom)throws Exception;
	
//	结算券操作
	public void insertSettlementOperation(SettlementOperationCustom settlementOperationCustom)throws Exception;
	
}
