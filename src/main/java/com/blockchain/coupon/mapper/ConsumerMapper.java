package com.blockchain.coupon.mapper;

import java.util.List;

import com.blockchain.coupon.po.ConsumerCoupon;
import com.blockchain.coupon.po.ConsumerCustom;

public interface ConsumerMapper {

//	消费者注册
	public void insertConsumer(ConsumerCustom consumerCustom)throws Exception;
	
//	获取消费者合约地址
	public String queryConsumerConAddr(String id)throws Exception;

	public ConsumerCustom queryConsumerInfoByAccount(String account);

	public void updateToken(ConsumerCustom mc);

	public List<ConsumerCoupon> consumerQueryUnusedCoupons(String consumerId);
	
}
