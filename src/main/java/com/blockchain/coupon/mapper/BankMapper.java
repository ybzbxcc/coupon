package com.blockchain.coupon.mapper;

import com.blockchain.coupon.po.BankStaffCustom;

public interface BankMapper {
	
//	根据银行职员id查询其信息
	public BankStaffCustom  queryBankStaffById(String id)throws Exception;
	

//	注册员工
	public void insertBankStaff(BankStaffCustom bankStaffCustom)throws Exception;
	
//	根据员工账户查询其信息
	public BankStaffCustom queryBankStaffByAccount(String account)throws Exception;
}
