package com.blockchain.coupon.mapper;

import java.util.List;

import com.blockchain.coupon.po.CouponRulerCustom;
import com.blockchain.coupon.po.MerchantCustom;
import com.blockchain.coupon.po.MerchantRegisterCustom;

public interface MerchantMapper {

//	查询商户结算券余额
	public Integer querySettlementBalance(String merchantId)throws Exception;

	
//	更新商户结算券余额
	public void updateSettlementBalance(MerchantCustom merchantCustom)throws Exception;
	
	
//	商户注册
	public void insertRegisterApp(MerchantRegisterCustom merchantRegisterCustom)throws Exception;
	
//	根据商户用户名查询商户信息
	public MerchantCustom queryMerchantInfoByAccount(String account)throws Exception;
	
//	根据商户用户名查询商户信息
	public MerchantRegisterCustom queryMerchantRegInfoByAccount(String account)throws Exception;
	
//	更新商户的token
	public void updateToken(MerchantCustom merchantCustom)throws Exception;

	
//	查询商户最近的发行规则id
	public String queryConCurrentCouponRulerId(String merchantId)throws Exception;
	
//	更新商户的当前发行规则id
	public void updateConCurrentCouponId(CouponRulerCustom couponRulerCustom)throws Exception;
	
//	查询注册信息尚未审核的商户
	public List<MerchantRegisterCustom> queryUncheckMerchant()throws Exception;
	
//	更新商户注册申请表状态
	public void updateMerchantRegisterStatus(MerchantRegisterCustom merchantRegisterCustom)throws Exception;
	
//	根据商户注册申请表id，查询注册表中的信息
	public MerchantRegisterCustom queryMerInfoFromRegisterById(String id)throws Exception;
	
//	向商户表中出入一条记录
	public void insertMerchant(MerchantCustom merchantCustom)throws Exception;
	
//	查询商户的公钥和合约地址
	public MerchantCustom queryPKAndConAddr(String id)throws Exception;
}
