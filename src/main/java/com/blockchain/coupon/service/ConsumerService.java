package com.blockchain.coupon.service;

import java.util.List;
import java.util.Map;

import com.blockchain.coupon.po.ConsumerCoupon;
import com.blockchain.coupon.po.ConsumerCustom;
import com.blockchain.coupon.po.CouponApplicationCustom;

public interface ConsumerService {

//	注册消费者
	public String consumerRegister(ConsumerCustom consumerCustom)throws Exception;

	public Map<String, String> consumerLogin(String account,String password) throws Exception;

	public List<ConsumerCoupon>consumerQueryUnusedCoupons(String consumerId);
	
//	插入优惠券申请
	public String insertCouponApplication(CouponApplicationCustom couponApplicationCustom)throws Exception;
	
//	添加优惠券支付申请
	 public String insertCouponPayApp(String merchantId, String consumerId, String couponIds)throws Exception;
}
