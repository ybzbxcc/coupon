package com.blockchain.coupon.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blockchain.coupon.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.blockchain.coupon.po.CouponApplicationCustom;
import com.blockchain.coupon.po.CouponCountInfo;
import com.blockchain.coupon.po.CouponInfo;
import com.blockchain.coupon.po.CouponPayApplicationCustom;
import com.blockchain.coupon.po.CouponRulerCustom;
import com.blockchain.coupon.po.MerchantRegisterCustom;
import com.blockchain.coupon.po.QueryCouponStatus;
import com.blockchain.coupon.po.SettlementOperationCustom;

@Controller
@RequestMapping(value="/merchant")
public class MerchantController {
	
	@Autowired
	private MerchantService merchantService;

//	申请结算券
	@RequestMapping(value="/applySettlementCoupon.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String>  applySettlementCoupon(SettlementOperationCustom soc) throws Exception {
		soc.setOperationType("1");
		String str = merchantService.insertSettlementOperation(soc);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", str);
		return m;
	}
	
//	查询结算券余额
	@RequestMapping(value="/querySettlementBalance.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, Integer>  querySettlementBalance(String id) throws Exception {
		Map<String, Integer> m=new HashMap<String, Integer>();
		int balance=0;
		balance=merchantService.querySettlementBalance(id);
		m.put("settlementBalance", balance);
		return m;
	}
	
//	商户注册
	@RequestMapping(value="/register.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> register(MerchantRegisterCustom merchantRegisterCustom)throws Exception{
		Map<String, String> m=new HashMap<String, String>();
		String resultCode=merchantService.insertMerchant(merchantRegisterCustom);
		m.put("resultCode", resultCode);
		return m;
	}
	
	
//	商户登录
	@RequestMapping(value="/login.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> login(String account,String password)throws Exception{
		return merchantService.login(account, password);
	}
	
//	结算券提现申请
	@RequestMapping(value="/applyWDApp.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String>  applyWDApp(SettlementOperationCustom soc) throws Exception {
		soc.setOperationType("2");
		String str = merchantService.insertSettlementOperation(soc);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", str);
		return m;
	}
	
//	优惠券发行
	@RequestMapping(value="/issueCoupons.action",method=RequestMethod.POST)
	public @ResponseBody Map<String, String> issueCoupons(CouponRulerCustom crc)throws Exception{
		String resultCode=merchantService.insertCouponRuler(crc);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;
	}
	
//	查询商户当前发行的优惠券状态
	@RequestMapping(value="/queryCouponsStatus.action",method=RequestMethod.POST)
	public @ResponseBody List<CouponInfo> queryCouponsStatus(QueryCouponStatus queryCouponStatus)throws Exception{
		return merchantService.queryCouponStatus(queryCouponStatus);
	}
	
//	查询已使用和未使用的优惠券
	@RequestMapping(value="/queryCouponsTotal.action", method=RequestMethod.GET)
	public @ResponseBody
    CouponCountInfo queryCouponsTotal(String id)throws Exception{
		return merchantService.queryTotalCoupons(id);
	}
	
//	查询优惠券申请信息
	@RequestMapping(value="/queryCouponApp.action", method=RequestMethod.GET)
	public @ResponseBody List<CouponApplicationCustom> queryCouponApp(String id)throws Exception{
		return merchantService.queryCouponApplication(id);
	}
	
//  获取优惠券额度
	@RequestMapping(value="/queryCouponValue.action", method=RequestMethod.POST)
	public @ResponseBody Map<String, Integer> queryCouponValue(String merchantId,String consumptionValue)throws Exception{
		Map<String, Integer> m=merchantService.getCouponValue(merchantId, consumptionValue);
		return m;
	}
	
//	查询优惠券支付申请
	@RequestMapping(value="/queryCouponPayApp.action", method=RequestMethod.GET)
	public @ResponseBody List<CouponPayApplicationCustom> queryCouponPayApp(String id)throws Exception{
		return merchantService.queryCouponPayApp(id);
	}
	
//	处理优惠券申请请求
	@RequestMapping(value="/updateCouponApp.action", method=RequestMethod.POST)
	public @ResponseBody Map<String, String> updateCouponApp(CouponApplicationCustom couponApplicationCustom)throws Exception{
		String resultCode=merchantService.dealCouponApplicaiton(couponApplicationCustom);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;
	}
	
//	处理优惠券使用请求
	@RequestMapping(value="/updateCouponPayApp.action", method=RequestMethod.POST)
	public @ResponseBody Map<String, String> updateCouponPayApp(String applicationCode,String id)throws Exception{
		String resultCode=merchantService.dealCouponPayApp(id, applicationCode);
		Map<String, String> m=new HashMap<String, String>();
		m.put("resultCode", resultCode);
		return m;
	}
}

















